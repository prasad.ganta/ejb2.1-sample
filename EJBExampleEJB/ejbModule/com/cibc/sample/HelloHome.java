package com.cibc.sample;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

public interface HelloHome  extends EJBHome{
	HelloObject create() throws CreateException,RemoteException;

}
