package com.cibc.sample;

import java.rmi.RemoteException;


import javax.ejb.EJBObject;

public interface HelloObject extends EJBObject {

	public String sayHello() throws RemoteException;

}
